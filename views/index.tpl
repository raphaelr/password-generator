<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Password Generator</title>
        <link rel="stylesheet" href="{{application_path}}s/site.css">
    </head>
    <body>
        <div id="root">
            <header>
                <hgroup>
                    <h1>Password Generator</h1>
                </hgroup>
            </header>
            <article>
                <pre>{{"\n".join(passwords)}}</pre>

                <p>
                    <strong>Entropy:</strong> {{"{:.1f}".format(entropy)}} bits
                    <form>
                        <table>
                            <tr>
                                <td><label for="numsyms">Symbols per password:</label></td>
                                <td><input type="number" id="numsyms" name="numsyms" value="{{numsyms}}"></label></td>
                            </tr>
                            <tr>
                                <td><label for="count">Password count:</label></td>
                                <td><input type="number" id="count" name="count" value="{{count}}"></label></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="submit" value="Generate passwords"></td>
                            </tr>
                        </table>
                    </form>
                </p>
            </article>
        </div>
    </body>
</html>




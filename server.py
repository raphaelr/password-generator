#!/usr/bin/env python
import random
import os
from bottle import Bottle, request, response, static_file, view
import passgen

app = Bottle()
symbols = passgen.getsymbols("dictionary")
application_path = os.getenv("APPLICATION_PATH", "/")

if not application_path.endswith("/"):
    application_path += "/"

@app.route("/")
@view("index")
def index():
    numsyms = int(request.query.get("numsyms", 5))
    count = int(request.query.get("count", 10))

    entropy = passgen.entropy(symbols, numsyms)
    passwords = passgen.samplen(symbols, numsyms, count)

    return {
        "entropy": entropy,
        "passwords": passwords,
        "numsyms": numsyms,
        "count": count,
        "application_path": application_path
    }

if application_path == "/":
    root_app = app
else:
    root_app = Bottle()
    root_app.mount(application_path, app)

if __name__ == '__main__':
    static_root = os.path.join(os.path.abspath(os.path.dirname(__file__)), "static")

    @app.route("/s/<filename>")
    def static(filename):
        return static_file(filename, static_root)

    root_app.run(host='0.0.0.0', port=9000, debug=True)

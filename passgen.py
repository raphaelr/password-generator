#!/usr/bin/env python
from math import log2
from random import SystemRandom

def entropy(symbols, numsyms):
    return log2(len(symbols) ** numsyms)

def sample1(symbols, numsyms, rng=SystemRandom()):
    return " ".join([rng.choice(symbols) for _ in range(0, numsyms)])

def samplen(symbols, numsyms, count, rng=SystemRandom()):
    return [sample1(symbols, numsyms, rng) for _ in range(0, count)]

def sample(count, numsyms=5, dictfile='dictionary'):
    symbols = getsymbols(dictfile)
    return samplen(symbols, numsyms, count)

def getsymbols(dictfile):
    with open(dictfile) as f:
        return list(set([stripped for line in f.readlines() for stripped in [line.strip().lower().replace("'", "")] if len(stripped)]))

if __name__ == '__main__':
    symbols = getsymbols('dictionary')
    print("entropy: {:.1f} bits".format(entropy(symbols, 5)))
    print("============================")
    for password in samplen(symbols, 5, 10):
        print(password)
